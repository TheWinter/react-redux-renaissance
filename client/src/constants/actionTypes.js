export const CREATE = 'CREATE';
export const UPDATE = 'UPDATE';
export const DELETE = 'DELETE';
export const FETCH_ALL = 'FETCH_ALL';
export const LIKE = 'LIKE';
export const AUTH = 'AUTH';
export const LOGOUT = 'LOGOUT';
export const CREATE_UPLOAD = 'CREATE_UPLOAD';
export const FETCH_UPLOADS = 'FETCH_UPLOADS';
export const FETCH_NOTIFS = 'FETCH_NOTIFS';
export const CREATE_NOTIF = 'CREATE_NOTIF';


