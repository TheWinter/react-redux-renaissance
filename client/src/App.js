import React from 'react';
import NavBar from './components/Navbar/Navbar';
import Home from './components/Home/Home';
import Auth from './components/Auth/Auth';
import Index from './components/Index/Index';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import './styles.css';

const App = () => {
    return (
        <BrowserRouter>
            <NavBar />
                <Switch>
                    <Route path="/index" exact component={Index} />
                    <div className="container">
                        <Route path="/" exact component={Home}/>
                        <Route path="/auth" exact component={Auth}/>
                    </div>
                </Switch>
        </BrowserRouter>
    )
}

export default App;