import * as api from '../api'
import { CREATE_UPLOAD, FETCH_UPLOADS } from '../constants/actionTypes'

export const getUploads = () => async (dispatch) => {

    try {
        const {data} = await api.getUploads();
        const jsdata = JSON.parse(data)
        dispatch({type: FETCH_UPLOADS, payload: jsdata})
    } catch (error) {
        console.log(error)
    }

}

export const postUpload = (upload) => async (dispatch) => {
    // 1. send upload data from front end to the backedn routes
    try {
        const { data } = await api.postUpload(upload)
        // we are expecting an object with filename and filepath propertyes
        console.log('data: ', data)
    // 2. take this data from backend and update redux state
        dispatch({ type: CREATE_UPLOAD, payload: data })
    } catch (error) {
        console.log(error)
    }

}


// export const createEvent = (event) => async (dispatch) => {
//     try {
//         const { data } = await api.createEvent(event);

//         dispatch({ type: CREATE, payload: data })

//     } catch (error) {
//         console.log(error);
//     }
// }