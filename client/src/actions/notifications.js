
import { FETCH_NOTIFS, CREATE_NOTIF } from '../constants/actionTypes';
import * as api from '../api'

export const getNotifs = () => async (dispatch) => {

    try {
        const { data } = await api.fetchNotifs();
        console.log('notif data: ', data)
        dispatch({type: FETCH_NOTIFS, payload: data})
    } catch (error) {
        console.log(error)
    }
}

export const createNotif = (notif) => async (dispatch) => {
    try {
        // await stalls JS, stops it from assigning a value to 'data' until the promise has resolved.
        // once resolved, the rsolved promise data is assigned to 'data' var.
        const { data } = await api.createNotif(notif);

        dispatch({ type: CREATE_NOTIF, payload: data })

    } catch (error) {
        console.log(error);
    }
}