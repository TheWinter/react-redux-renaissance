import { AUTH } from '../constants/actionTypes';
import * as api from '../api'

export const signin = (formData, history) => async (dispatch) => {
    try {
        
        // log in user
        const { data } = await api.signIn(formData);
        console.log("got data from action ", data)
        dispatch({ type: AUTH, data})
        history.push('/');
    } catch (error) {
        console.log(error);
    }
}

export const signup = (formData, history) => async (dispatch) => {
    try {
        console.log("signup action start")
        const { data } = await api.signUp(formData);
        console.log("got data from action signup ", data)
        dispatch({ type: AUTH, data})
        history.push('/');
    } catch (error) {
        console.log(error);
    }
}