import React, { useState, useEffect } from 'react';
import Events from '../../components/Events/Events'
import Notifications from '../Notifications/Notifications';
import Form from '../../components/Form/Form';
import FormNot from '../../components/FormNot/FormNot';
import Upload from '../../components/Upload/Upload';
import { useDispatch } from 'react-redux';
import { getEvents } from '../../actions/events'
import { getNotifs } from '../../actions/notifications'


const Home = () => {
    const [currentId, setCurrentId] = useState(null);
    const dispatch = useDispatch();

    // here we make call to get all posts. But, it seems like we need another step to pass the info to ccs, which
    // needs to be written in these ccs. namely useSelector.
    useEffect(() => {
        dispatch(getEvents())
        // adding currentId below means, as soon as we change currentId in the app, the app will dispatch getPosts action.
    }, [currentId, dispatch]);

    useEffect(() => {
        dispatch(getNotifs());

    }, [dispatch]);

    return (
        <div className="row">
            <div className="col-sm-6">
                <Form currentId={currentId} setCurrentId={setCurrentId}/ >
                <Events setCurrentId={setCurrentId}/ >
            </div>
            <div className="col-sm-6">
                <FormNot />
                <Notifications />
                <Upload />
            </div>
        </div>
    )
}

export default Home
