import React, { useState, useEffect } from 'react';
import { Link, useHistory, useLocation } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import './styles.css';

const NavBar = () => {
    // state retrieved from localStorage
    const [user, setUser] = useState(JSON.parse(localStorage.getItem('profile')))
    const dispatch = useDispatch();
    const history = useHistory();
    const location = useLocation();
    

    const logout = () => {
        dispatch({ type: 'LOGOUT'});

        history.push('/');

        setUser(null);
    }

    // when call this useEffect? (this necessary to auto refresh navbar once logged in)
    // when url changes from '/auth' to '/'
    // get this from 'location' var
    useEffect(() => {
        // check token exists
        const token = user?.token;
        // console.log('user what change?', user, token)
        // JWT

        setUser(JSON.parse(localStorage.getItem('profile')))
    }, [location]);
    return (
        <nav class="navbar navbar-expand-lg colors">
            <div class="container">
                <a class="navbar-brand heading" href="#">Renaissance Woman</a>
                <button
                class="navbar-toggler"
                type="button"
                data-mdb-toggle="collapse"
                data-mdb-target="#navbarNavAltMarkup"
                aria-controls="navbarNavAltMarkup"
                aria-expanded="false"
                aria-label="Toggle navigation"
                >
                <i class="fas fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-link active text-white" aria-current="page" href="#">Home</a>
                    {user ? (
                    <div >
                        {/* <img src={user.result.imageUrl} alt={user.result.name} /> */}
                        <h4>{user.result.name}</h4>
                        <button className="auth-btn" onClick={logout}>Logout</button>
                    </div>
                    ) : (
                        <div className="auth-btn">
                            {/* // redirect to auth page */}
                            <Link to="/auth">Sign In</Link>
                        </div>
                    )}
                    {/* <a class="nav-link" href="#">Features</a>
                    <a class="nav-link" href="#">Pricing</a>
                    <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true"
                    >Disabled</a
                    > */}
                </div>
                </div>
            </div>
        </nav>
        // <nav class="navbar navbar-expand-lg navbar-container">
        //     <div className="heading-div">
        //         <h4 className="heading">Renaissance Woman </h4>
        //         {/* <p></p> */}
        //     </div>                
        //     <div>
        //         {user ? (
        //             <div >
        //                 <img src={user.result.imageUrl} alt={user.result.name} />
        //                 <h4>{user.result.name}</h4>
        //                 <button className="auth-btn" onClick={logout}>Logout</button>
        //             </div>
        //         ) : (
        //             <div className="auth-btn">
        //                 {/* // redirect to auth page */}
        //                 <Link to="/auth">Sign In</Link>
        //             </div>
        //         )}
        //     </div>
        // </nav>
    )
}

export default NavBar;