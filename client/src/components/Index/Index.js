import React from 'react'
import './styles.css'
import RW from "../../images/RW.jpg"
import RWTwo from "../../images/RW2.jpg"
import RWThree from "../../images/RW3.jpg"


function Index() {
    return (
        <div >
            {/* <!-- Background image --> */}
            <div
                className="p-5 text-center bg-image intro" id="intro"
            >
                <div className="mask" style={{backgroundColor: 'rgba(0, 0, 0, 0.6)'}}>
                <div className="d-flex justify-content-center align-items-center h-100">
                    <div className="text-white">
                    <h1 className="mb-3">Renaissance Woman</h1>
                    <h4 className="mb-3">A society for celebrating and sharing artistic endeavours</h4>
                    <a className="btn btn-outline-light btn-lg mt-3" href="#!" role="button"
                        >Call to action</a
                    >
                    </div>
                </div>
                </div>
            </div>
            {/* <!-- Background image --> */}
            <main>
                <div className="container-fluid p-0">
                    <section className="divider py-5">
                        <div className="row justify-content-center align-items-center text-white">
                            <div className="col-md-4 p-20 text-right ">
                                <h2 className="px-4">About Us</h2>
                            </div>
                            <div className="col-md-4">
                                <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam feugiat ex quis sem volutpat imperdiet. Ut a congue magna. In hac habitasse platea dictumst. Duis consectetur elit consequat mi aliquet mollis. Cras sed erat cursus, mattis massa vel, dignissim sem. Maecenas sed enim sit amet nibh efficitur condimentum quis et ante.</p>
                            </div>
                        </div>
                    </section>
                    <section id="events">
                        <div className="row justify-content-center align-items-center">
                            <div className="col-md-4">
                            <div id="carouselExampleSlidesOnly" class="carousel slide" data-mdb-ride="carousel">
                                <div class="carousel-inner" style={{width: '350px'}}>
                                    <div class="carousel-item active">
                                    <img
                                        src={RW}
                                        class="d-block w-100"
                                        alt="..."
                                    />
                                    </div>
                                    <div class="carousel-item">
                                    <img
                                        src={RWTwo}
                                        class="d-block w-100"
                                        alt="..."
                                    />
                                    </div>
                                    <div class="carousel-item">
                                    <img
                                        src={RWThree}
                                        class="d-block w-100"
                                        alt="..."
                                    />
                                    </div>
                                </div>
                            </div>

                            </div>
                            <div className="col-md-4 text-center px-3 text-black">
                                <h2 className="my-3">Events</h2>
                                <hr></hr>
                                <div className="mb-3">
                                    <h4 >Event I</h4>
                                    <p><b>6pm Weekday Day Month @ Location Rd</b></p>

                                    <p>Donec sollicitudin finibus ex pharetra tincidunt. Sed nec gravida ex. Pellentesque eros odio, scelerisque vel 
                                        nunc in, sodales blandit leo. Interdum et malesuada fames ac ante ipsum primis in faucibus. 
                                        Donec bibendum nibh id libero blandit cursus. Integer non sapien cursus, lacinia diam eu, sagittis ipsum.
                                        Pellentesque pharetra ultrices tellus rutrum lobortis. </p>
                                </div>
                                <div className="my-4">
                                    <h4>Event II</h4>
                                    <p><b>6pm Weekday Day Month @ Location Rd</b></p>

                                    <p>Praesent fermentum nulla nisl, non interdum ipsum pretium id. Ut risus dui, 
                                        dictum et ante at, posuere dictum metus. Ut convallis vel dolor ut egestas. </p>
                                </div>
                                <div className="my-3">
                                    <h4>Event III</h4>
                                    <p><b>6pm Weekday Day Month @ Location Rd</b></p>

                                    <p>Nam tincidunt turpis vel urna cursus sodales sit amet eget mauris. Sed varius 
                                        erat nibh, vel egestas dui tempus sit amet. Vestibulum vel sem faucibus, mollis 
                                        dolor non, lobortis neque. Vivamus sollicitudin felis sed lectus egestas porta. </p>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </main>
            <hr/>
            <div className="row">
                <div className="col-sm-4">

                </div>
                <div className="col-sm-4">
                    {/* <h1>Events</h1> */}

                </div>
            </div>
        </div>
    )
}

export default Index

