import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import {  createNotif } from '../../actions/notifications'
import './styles.css'

function FormNot() {
    const dispatch = useDispatch();
    const user = JSON.parse(localStorage.getItem('profile')) // how we get the user
    
    const [show, setShow] = useState(false);
    const [notifData, setNotifData] = useState({
        content: '',
        date: new Date()
    })

    const handleSubmit = (e) => {
        e.preventDefault();

        dispatch(createNotif(notifData))
        setShow(false)

        // clearHack();
    }


    return (
        <div className="form-layout">
            { show ?  
            <form autoComplete="off" noValidate onSubmit={handleSubmit}>
                    <h3 className={''}> Create a Notification</h3>
                    <a href="#" onClick={() => setShow(false)}>x</a>
                    <hr/>
                    <div className="form-group">
                        <input 
                        className="form-control"
                        name="content" 
                        label="Content" 
                        value={notifData.content}
                        // placeholder="Event Name"
                        onChange={(e) => setNotifData({...notifData, content: e.target.value})} // spread postData first, to preserve info in rest of eventData object
                        />
                    </div>

                    <button 
                    // className={classes.buttonSubmit} 
                    color="primary"
                    size="large"
                    type="submit"
                >Create Notification</button>
                </form>
                :
                <a className="show" onClick={()=>setShow(true)}>Create a Notification</a>
        }
            
        </div>
    )
}

export default FormNot
