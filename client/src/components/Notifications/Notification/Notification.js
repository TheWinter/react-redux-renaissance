import React from 'react';
import moment from 'moment';

function Notification({ notif }) {
    return (
        <div class="card">
        <div class="card-body">
          <h5 class="card-title">{notif.content}</h5>

            <p className="text-end" >Posted by: {notif.creatorReplace[0].name}</p>
        </div>
      </div>
    )
}

export default Notification
