import React from 'react';
import Notification from './Notification/Notification';
import { useSelector } from 'react-redux';


function Notifications() {
    const notifs = useSelector((state) => state.notifications)
    return (
        <div className="events-container"> 
        <div class="accordion" id="">
           <div className="events-header">
               <h3> Notifications</h3>
               <h5>{notifs.length} Notifications Listed</h5>
           </div>
           {notifs.map((notif) => (
               <Notification notif={notif} key={notif._id}  />
       ))}
       </div>
   </div>
    )
}

export default Notifications
