import React, { useState } from 'react'
// import LockOutlinedIcon from '@material-ui/icons/LockOutlinedIcon';
import Input from './Input';
import { useDispatch } from 'react-redux';
import { useHistory } from "react-router-dom";
import { signin, signup } from '../../actions/auth'


const initialState = {firstName: '', lastName: '', email: '', password: '', confirmPassword: ''}
const Auth = () => {

    const [isSignup, setIsSignup] = useState(false)
    const [showPassword, setShowPassword] = useState(false);
    const dispatch = useDispatch();
    const history = useHistory();

    // like we did for postData
    const [formData, setFormData] = useState(initialState);

    // whenever you are changing the state using the old state, you need to pass a callback function to setter
    // then pass prev, and change to opposite
    const handleShowPassword = () => setShowPassword((prevShowPassword) => !prevShowPassword)
    const handleSubmit = (e) => {
        e.preventDefault();

        if (isSignup) {
            console.log("issignup handler fe")
            dispatch(signup(formData, history))
        } else {
            dispatch(signin(formData, history))
        }
    }

    const handleChange = (e) => {
        setFormData({ ...formData, [e.target.name]: e.target.value })
    }
    
    const switchMode = () => {
        setIsSignup((prevIsSignup) => !isSignup)
    }

    return (
        <div>
            {/* <LockOutlinedIcon /> */}
            <h5>{isSignup ? 'Sign Up' : 'Sign In'}</h5>
            <form onSubmit={handleSubmit} noValidate>
                <div>
                    {
                        isSignup && (
                            <>
                                <Input name="firstName" type="text" label="First Name" className="form-control" handleChange={handleChange} placeholder="First Name" autoFocus />
                                <Input name="lastName" type="text" label="First Name" className="form-control" handleChange={handleChange} placeholder="First Name" autoFocus />
                            </>
                            )
                        }
                        <Input name="email" type="email" label="Email" className="form-control" handleChange={handleChange} />
                        <Input name="password" type={showPassword ? "text" : "password"} handleShowPassword={handleShowPassword} label="Password" className="form-control" handleChange={handleChange} />
                    { isSignup && <Input name="confirmPassword" label="Repeat Password" handleChange={handleChange} type="password" />

                    }
                    <button type="submit" >
                        { isSignup ? 'Sign Up' : 'Sign In' }
                    </button>
                </div>

            </form>
                    <button onClick={switchMode}>
                        { isSignup ? 'Already have an account?' : "Don't have an account?"}
                    </button>
        </div>
    )
}

export default Auth
