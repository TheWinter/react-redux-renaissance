import React from 'react'

function Input({name, handleChange, label, autoFocus, type}) {
    return (
        <div>
            {/* // TODO make show password prop 
            tutorial used InputProps, endAdornment, InputAdornment */}
            <input 
                name={name}
                onChange={handleChange}
                required
                label={label}
                type={type}
                autoFocus={autoFocus}
            />
        </div>
    )
}

export default Input
