import React from 'react';
import DeleteIcon from '@material-ui/icons/Delete'
import MoreHorizIcon from '@material-ui/icons/MoreHoriz'
import StarRateOutlinedIcon from '@material-ui/icons/StarRateOutlined';
import moment from 'moment';
import './styles.css';
import { useDispatch } from 'react-redux';
import { deleteEvent, likeEvent } from '../../../actions/events'

const Event = ({event, setCurrentId }) => {
    const dispatch = useDispatch();
    const user = JSON.parse(localStorage.getItem('profile')) // how we get the user

    const Likes = () => {
        if (event.likes.length > 0) {
            return event.likes.find((like) => like === (user?.result?._id))
            ? (
                <>{ user?.result?.name }</>
            ) : (
                <>something else here</>
            )
        }
        return <p>0 Going</p>
    }

    return (      
        <div className="event-container collapse" id="collapseOne" aria-labelledby="headingOne" data-parent="#accordionExample">
            <div className="event-header" >
                {event.likes.find((like) => like === (user?.result?._id)) ? (
                    <div>
                      <p className="confirmed-check" >CONFIRMED</p>
                    </div>
                    ) : (
                    <h5 className="blinking">Awaiting Response...</h5>
                )}
                <div>
                    <button 
                    size="small" 
                    onClick={() => setCurrentId(event._id)}>
                        <MoreHorizIcon fontSize="default" />
                    </button>
                    <button size="small" color="primary" onClick={() => dispatch(deleteEvent(event._id))}><DeleteIcon fontSize="small" /> Del</button>
                </div>
            </div>
            <div className="event-details">
                {/* <div> */}
                    <p className="details-title">Name:</p><h6> {event.name}</h6>
                    <p className="details-title" >Location:</p><h6> {event.location}</h6>
                    <p className="details-title">Time & Date:</p><h6> {moment(event.date).fromNow()}</h6>
                {/* </div> */}
            </div>
            <div className='event-actions-members'>
                <button size="small" color="primary" disabled={!user?.result} onClick={() => dispatch(likeEvent(event._id))}>{event.likes.find((like) => like === (user?.result?._id)) ? "Cancel" : "Join"}
                </button>
            </div>
            <hr/>
            <div>
                {event.likes.find((like) => like === (user?.result?._id)) ? (
                    <p>Thank you for confirming your participation! </p>
                ) : (
                    <p>You are not participating in this event</p>
                )}
                <h5>{`${event.likes.length} Participating`}</h5>
                <p>
                <a data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                    Show Participants
                </a>
                </p>
                <div class="collapse" id="collapseExample">
                <div class="card card-body">
                    <Likes />                
                </div>
                </div>
            </div>
        </div>
    )
}

export default Event;