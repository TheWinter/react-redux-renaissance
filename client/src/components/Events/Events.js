import React from 'react';
import { useSelector } from 'react-redux';
import Event from './Event/Event'
import EventNoteSharpIcon from '@material-ui/icons/EventNoteSharp';
import './styles.css';
import posts from '../../reducers/events';

const Events = ({ setCurrentId }) => {
    // i think this is paired with the getEvents action creator dispatched from App.js
    // 'events' in state.events comes from combineReducers
    const events = useSelector((state) => state.events)

    console.log('Events component: ', events)
    return (
        !events.length ? <div>Progress...</div> : ( 
            <div className="events-container"> 
                 <div class="accordion" id="accordionExample">
                    <div className="events-header" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <h3> <EventNoteSharpIcon /> Upcoming Events</h3>
                        <h5>{events.length} Events Listed</h5>
                    </div>
                    {events.map((event) => (
                        <Event event={event} key={event._id} setCurrentId={setCurrentId} />
                ))}
                </div>
            </div>

        )
    )
}

export default Events;