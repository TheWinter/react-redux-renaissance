import React, { useState, useEffect } from 'react';
import CreateIcon from '@material-ui/icons/Create';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import {
    DateTimePicker
} from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import { useDispatch, useSelector } from 'react-redux';
import { createEvent, updateEvent } from '../../actions/events'
import './styles.css';

const Form = ({ currentId, setCurrentId }) => {
    const dispatch = useDispatch();

    // currentID must be id of the event.
    const event = useSelector((state) => currentId ? state.events.find((p) => p._id === currentId) : null)
    const user = JSON.parse(localStorage.getItem('profile')) // how we get the user
    const [eventData, setEventData] = useState({
        name: '',
        location: '',
        date: new Date()
    })



    // if we have event (meaning we have event id), set the eventData to that event
    useEffect(() => {
        if (event) setEventData(event);

         // when value of evenr changes (from nothing to havign a post, i guess?)
    }, [event])

    const handleDateChange = (date) => {
        // setSelectedDate(date)
        setEventData({...eventData, date: date})
    }


    // dispatch post action in handlesubmit, because we're posting it on form submit.
    const handleSubmit = (e) => {
        e.preventDefault();

        // updating if currentId meaning??
        if (currentId) {
            dispatch(updateEvent(currentId, eventData))
        } else {
            dispatch(createEvent(eventData))
        }

        clearHack();
    }

    const clear = (e) => {
        e.preventDefault();
        setCurrentId(null);
        setEventData({
            name: '',
            location: '',
            date: new Date()
        })
    }

    const clearHack = () => {
        setCurrentId(null);
        setEventData({
            name: '',
            location: '',
            date: new Date()
        })
    }
    console.log("user... ", user)
    if (!user?.result?.name) {
        return (
        <div>
            <h2>Please sign in to perform this function</h2>
        </div>
        )
    }

    return (
        <div className="form-layout">
            <form autoComplete="off" noValidate onSubmit={handleSubmit}>
                <h3 className={''}> <CreateIcon /> {currentId ? 'Edit' : 'Create'} an Event</h3>
                <hr/>
                <div className="form-group">
                    <input 
                    className="form-control"
                    name="title" 
                    label="Title" 
                    value={eventData.name}
                    placeholder="Event Name"
                    onChange={(e) => setEventData({...eventData, name: e.target.value})} // spread postData first, to preserve info in rest of eventData object
                    />
                </div>
                <div className="form-group">
                    <input                                         
                    className="form-control"
                    name="location" 
                    label="Location" 
                    placeholder="Location"
                    value={eventData.location}
                    onChange = {(e) => setEventData({...eventData, location: e.target.value})}
                    />
                </div>
                <div className="form-group">
                 <MuiPickersUtilsProvider utils={MomentUtils}>
                    <DateTimePicker 
                    value={eventData.date} 
                    onChange={handleDateChange} />
                 </MuiPickersUtilsProvider>
                 </div>
                 <button 
                // className={classes.buttonSubmit} 
                color="primary"
                size="large"
                type="submit"
            >Create Event</button>
            <button onClick={clear}>Clear</button>
            </form>
        </div>
    )
}

export default Form;