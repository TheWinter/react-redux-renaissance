import React, { useState, useEffect } from 'react'
import { postUpload, getUploads } from '../../actions/uploads'
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from 'react-hook-form'
import StorageIcon from '@material-ui/icons/Storage';
import './styles.css'


const Upload = () => {
    const dispatch = useDispatch();
    const [file, setFile] = useState()
    const [filename, setFilename] = useState('Choose File');
    const uploads = useSelector((state) => state.uploads)

    useEffect(() => {
        dispatch(getUploads())

         // when value of evenr changes (from nothing to havign a post, i guess?)
    }, [dispatch])

    const onChange = e => {
        // with html file inputs you can do multiple files.
        // e.target.files is an array
        setFile(e.target.files[0]);
        setFilename(e.target.files[0].name)
    }

    const onSubmit = (e) => {
        e.preventDefault();

        // new formdata is not react, its just part of JS
        const formData = new FormData();

        // this 'file' pertains to the backend req.files.file.
        // if we called it img, then the req files would have img prop
        // Your input's name field is file: <input name="file" type="file" />

        formData.append('file', file)
        dispatch(postUpload(formData))

    } 
    return (
        <div>
            <h2><StorageIcon /> Library</h2>
            <form onSubmit={onSubmit}>
                <div className="custom-file">
                    <input type="file" className="custom-file-input" id="customFile" onChange={onChange}/>
                    <label className="custom-file-label" htmlFor="customFile">{filename}</label>
                </div>
                <button value="Upload">Upload</button>
            </form>
            <div className="library-contents">
               {uploads.map((upload) => (
                    <p>{upload}</p>
                ))}
            </div>
        </div>
    )
}

export default Upload;