import axios from 'axios';

const API = axios.create({ baseURL: 'http://localhost:5000'})

// provide backend with header
API.interceptors.request.use((req) => {
    if(localStorage.getItem('profile')) {
        // adding bearer, prepending the string to our token, and adding this to every request.
        req.headers.Authorization = `Bearer ${JSON.parse(localStorage.getItem('profile')).token}`
    }
    // return request so you can make all requests (below)
    return req;
})

export const fetchEvents =  () => API.get('/events');
export const createEvent = (newEvent) => API.post('/events', newEvent);
export const updateEvent = (id, updatedEvent) => API.patch(`/events/${id}`, updatedEvent)
export const deleteEvent = (id) => API.delete(`/events/${id}`)
export const likeEvent = (id) => API.patch(`/events/${id}/likeEvent`)

// Notifications
export const fetchNotifs = () => API.get('/notifications');
export const createNotif = (notif) => API.post('/notifications', notif);


// Uploads
export const getUploads = () => API.get('/uploads')
export const postUpload = (formData) => API.post('/uploads', formData, {
    headers: {
        'Content-Type': 'multipart/form-data'
    }
})

export const signIn = (formData) => API.post('/users/signin', formData)
export const signUp = (formData) => API.post('/users/signup', formData)