import { bindActionCreators } from "redux"
import { FETCH_ALL, CREATE, UPDATE, DELETE, LIKE } from '../constants/actionTypes';

// reducer is a function which accepts the state and action
// our posts will be in an array, thats why we set posts state 
// to []
const events = (events = [], action) => {
    console.log("event action is: ", action)
    //based on action type
    switch(action.type) {
        case FETCH_ALL:
            return action.payload; // our posts as defined in the action creator
        case CREATE:
            return [...events, action.payload]; 
        case UPDATE:
        case LIKE:
            return events.map((event) => (event._id === action.payload._id ? action.payload : event));
        case DELETE:
            return events.filter((post) => post._id !== action.payload )
        default:
            return events;
            // break
    }   
}

export default events;