import { bindActionCreators } from "redux"
import { CREATE_UPLOAD, FETCH_UPLOADS } from '../constants/actionTypes';

// reducer is a function which accepts the state and action
// our posts will be in an array, thats why we set posts state 
// to []
const uploads = (uploads = [], action) => {
    //based on action type
    switch(action.type) {
        case FETCH_UPLOADS:
            return action.payload; // our posts as defined in the action creator
        case CREATE_UPLOAD:
            return [...uploads, action.payload]; 
        // case UPDATE:
        // case LIKE:
        //     return events.map((event) => (event._id === action.payload._id ? action.payload : event));
        // case DELETE:
        //     return events.filter((post) => post._id !== action.payload )
        default:
            return uploads;
            // break
    }   
}

export default uploads;