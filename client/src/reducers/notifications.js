import { bindActionCreators } from "redux"
import { FETCH_NOTIFS, CREATE_NOTIF } from '../constants/actionTypes';

// reducer is a function which accepts the state and action
// our posts will be in an array, thats why we set posts state 
// to []
const notifications = (notifications = [], action) => {
    console.log("action is: ", action)
    //based on action type
    switch(action.type) {
        case FETCH_NOTIFS:
            return action.payload; // our posts as defined in the action creator
        case CREATE_NOTIF:
            return [...notifications, action.payload]; 
        default:
            return notifications;
            // break
    }   
}

export default notifications;