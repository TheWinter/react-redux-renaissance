import { combineReducers} from 'redux';

import events from './events';
import notifications from './notifications';
import authReducer from './auth';
import uploads from './uploads';

// combine reducers takes an object
// if key and value are the same, just write it once
export default combineReducers({
    events,
    notifications,
    authReducer,
    uploads
})