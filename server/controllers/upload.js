import fs from 'fs';

export const getUploads = (req, res) => {
        const directoryPath = './uploads';

        var files = fs.readdirSync(directoryPath)
        console.log("files: ", files)
        var jsonFiles = JSON.stringify(files);
        console.log("jsonfiles: ", jsonFiles)
        res.json(jsonFiles)
}

export const postUpload = (req, res) => {
    if (req.files === null) {
        return res.status(400).json({msg: 'No file uploaded'})
    }
    const file = req.files.file;

    file.mv(`./uploads/${file.name}`, err => {
        if(err) {
            console.log(err);
            return res.status(500).send(err);
        }

        res.json({ fileName: file.name, filePath: `/uploads/${file.name}`})
    })
}