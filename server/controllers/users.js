import bcrypt from 'bcryptjs'
import jwt from 'jsonwebtoken';

import User from "../models/user.js";

export const signin = async (req, res) => {
    const { email, password } = req.body;

    try {
        // does the user exist?
        const existingUser = await User.findOne({ email });
        // if not, let them know
        if (!existingUser) return res.status(404).json({ message: "User doesnt exist" });
        // check if password is correct (comparing hashed passwoeds)
        const isPasswordCorrect = await bcrypt.compare(password, existingUser.password)
        // if password incorrect, let them know
        if (!isPasswordCorrect) return res.status(400).json({ message: "Invalid credentials"})
        // if user exists and password correct
        // get their jsonwebtoken to send to front end
        // in sign, we provide all info we want stored in token
        // second argument is the secret string. From a separate env file
        const token = jwt.sign({ email: existingUser.email, id: existingUser._id }, process.env.JWTSTRING, {expiresIn: "1h"})
        // return user and token
        res.status(200).json({ result: existingUser, token })
    } catch (error) {
        res.status(500).json({ message: "Something went wrong"})
        console.log(error);
    }
}

export const signup = async (req, res) => {
    const { email, password, confirmPassword, firstName, lastName } = req.body;

    try {
        // try finding user
        const existingUser = await User.findOne({ email });
        // if user exists, let them know
        if(existingUser) return res.status(400).json({ message: 'User already exists'})
        // do the passwords match?
        if (password !== confirmPassword) return res.status(400).json({ message: "Passwords dont match"});
        // if these 2 pass, we can create user
        // first step is to hash the password
        // second arg, salt (level of difficulty of hashing, usually 12)
        const hashedPassword = await bcrypt.hash(password, 12)

        const result = await User.create({ email, password: hashedPassword, name: `${firstName}, ${lastName}` })
        // now the user is created, we need to create the token
        const token = jwt.sign({ email: result.email, id: result._id }, process.env.JWTSTRING, {expiresIn: "1h"})

        res.status(200).json({ result, token })

    } catch (error) {
        res.status(500).json({ message: "Something went wrong"})
        console.log(error);
    }
}

