import mongoose from "mongoose";
import Event from "../models/Event.js";

export const getEvents = async (req, res) => {
    try {
        // get all events
        const events = await Event.find({});
        res.status(200).json(events)

    } catch (error) {
        res.status(404).json(error)
    }
}

export const createEvent = async (req, res) => {
    const event = req.body;
    // creator is an id, not a name
    const newEvent = new Event({ ...event, creator: req.userId });

    try {
        await newEvent.save();

        res.status(201).json(newEvent);

    } catch (error) {
        res.status(409).json({ message: error.message })
    }
}

export const updateEvent = async (req, res) => {
    // renaming id to _id
    const { id: _id } = req.params;
    const event = req.body;

    // does this id exist in DB?
    if(!mongoose.Types.ObjectId.isValid(_id)) return res.status(404).send('No event with this id exists');

    // method takes ID and whole updated post
    // new tru to receive updated version of event (and store in var)
    const updatedEvent = await Event.findByIdAndUpdate(_id, { ...event, _id}, { new: true })

    res.json(updatedEvent)
}

export const deleteEvent = async (req, res) => {
    const { id: _id } = req.params;

    if(!mongoose.Types.ObjectId.isValid(_id)) return res.status(404).send('No event with that id')
    
    await Event.findByIdAndDelete(_id)

    res.json({ message: "event deleted successfully"})
}

export const likeEvent = async (req, res) => {
    const { id: _id } = req.params;

    // from auth middleware, we have populated req object with userId (it carries over to next route)
    // check if user is authenticated
    if(!req.userId) return res.json({ message: 'Unauthenticated'})

    if(!mongoose.Types.ObjectId.isValid(_id)) return res.status(404).send('No event with that id')

    // get the event from DB
    const event = await Event.findById(_id)
    console.log('req b EVENT: ', event, req.userId)

    // check if person has already liked
    // each like is an id from a specific person. Must convert to String
    const index = event.likes.findIndex((_id) => _id === String(req.userId))

    // if id is not in index var
    if (index === -1) {
        // like post
        event.likes.push(req.userId)
    } else {
        // remove the like
        event.likes = event.likes.filter((id) => id !== String(req.userId));

    }
    // update
    const updatedEvent = await Event.findByIdAndUpdate(_id, event, { new: true })

    res.json(updatedEvent);

}