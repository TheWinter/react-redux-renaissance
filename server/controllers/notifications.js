import Notification from '../models/Notification.js';
import User from '../models/user.js';

export const getNotifications = async (req, res) => {
    try {
        // get all notifs and send to front
        const notifs = await Notification.find({}).populate({ path: 'creatorReplace', model: 'User' });
        res.status(200).json(notifs)
        
    } catch (error) {
        res.status(404).json(error)
    }
}

export const postNotification = async (req, res) => {
    const notif = req.body;
    const newNotif = new Notification({ ...notif, creatorReplace: req.userId });

    try {
        await newNotif.save();

        res.status(201).json(newNotif);

    } catch (error) {
        res.status(409).json({ message: error.message })


    }
}