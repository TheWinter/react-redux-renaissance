import express from 'express';
import auth from '../middleware/auth.js';
import { getNotifications, postNotification } from '../controllers/notifications.js';
const router = express.Router();

router.get('/', getNotifications);
router.post('/', auth, postNotification);

export default router;