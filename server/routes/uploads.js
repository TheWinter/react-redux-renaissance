import express from 'express';
import { postUpload, getUploads } from '../controllers/upload.js'
const router = express.Router();

router.get('/', getUploads)
router.post('/', postUpload);

export default router;
