import express from 'express';
import { getEvents, createEvent, updateEvent, deleteEvent, likeEvent } from '../controllers/events.js';
import auth from '../middleware/auth.js';
const router = express.Router();

// atm you can see all events even when not logged in (might change this 1)
router.get('/', getEvents);
router.post('/', auth, createEvent);
router.patch('/:id',auth, updateEvent);
router.delete('/:id', auth, deleteEvent);
router.patch('/:id/likeEvent', auth, likeEvent);


export default router;