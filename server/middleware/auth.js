import jwt from 'jsonwebtoken';

const auth = async (req, res, next) => {
    try {
        // token is in the first position on authorization header
        const token = req.headers.authorization.split(" ")[1];
        const isCustomAuth = token.length < 500;

        // data we want to get from the token
        let decodedData;
        // if we have token and its our own
        // get data from token (username, id)
        if (token && isCustomAuth) {
            decodedData = jwt.verify(token, process.env.JWTSTRING)
            // now that we have this data we know which user is logged in.
            // we'll store their id in req.userId
            // ?. for the optional chaining
            req.userId = decodedData?.id
        }
        // move on to next action
        next();
    } catch (error) {
        console.log(error)
    }
}

export default auth;