import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import cors from 'cors';
import dotenv from 'dotenv';
import fileUpload from 'express-fileupload'

import eventRoutes from './routes/events.js'
import userRoutes from './routes/users.js'
import uploadRoutes from './routes/uploads.js'
import notificationRoutes from './routes/notifications.js'

const app = express();
dotenv.config();

app.use(bodyParser.json({ limit: "30mb", extended: true }))
app.use(bodyParser.urlencoded({ limit: "30mb", extended: true }))
app.use(cors());
app.use(fileUpload());

// Routes
app.use('/events', eventRoutes);
app.use('/users', userRoutes);
app.use('/uploads', uploadRoutes);
app.use('/notifications', notificationRoutes);

// TODO Error: listen EADDRINUSE: address already in use 5000; when exporting port from .env
const PORT = process.env.PORT || 5000;

mongoose.connect(process.env.CONNECTION_URL, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => app.listen(PORT, () => console.log(`Server running on port: ${PORT}`)))
    .catch((error) => console.log(error.message))

// makes sure no warnings in console
mongoose.set('useFindAndModify', false)
