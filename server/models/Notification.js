import mongoose from "mongoose";

const notificationSchema = new mongoose.Schema({
    content: String,
    data: { type: Date, default: Date.now },
    creator: String,
    creatorReplace: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }]
})

const Notification = mongoose.model('Notification', notificationSchema);

export default Notification;