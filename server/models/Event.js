import mongoose from "mongoose";

const eventSchema = new mongoose.Schema({
    name:  String, // String is shorthand for {type: String}
    location: String,
    date: { type: Date, default: Date.now },
    creator: String,
    likes: {
        type: [String], // arr of strings (id's)
        default: [] // when object like this, is default the thing we update?
    },
  });

  const Event = mongoose.model('Event', eventSchema);

  export default Event;