## Description
An in-progress remake of an older project (full stack website used by a community choir), using some different technologies. 

- React
- Redux
- Redux Thunk
- REST API
- JWT authentication
- MongoDB Atlas
- NodeJS and Express

Screenshots Below.

## Features
- User registration and log in
- Check user authenticated on different routes
- Users can confirm or cancel participation of events
- Users will be able to see all others who are participating in event (as in choir website)
- Uploading of files to website

### Issues
- Change styling to conform with Material design principles
- Make Index page the primary one when logged out
- Add notifications
- Various TODO's as still in progress

### Credits
Thanks to JavaScript Mastery, which made it easy for me to code along with their tutorial and help build this project. 
Link to github repo:
https://github.com/adrianhajdin/project_mern_memories

### Screenshots

Main page - in two parts because the text in Events text got cut off on full screenshot.
![Alt text](/client/src/images/full_page.png?raw=true)
Events section screenshot with text
![Alt text](/client/src/images/events_section.png?raw=true)

Dashboard
![Alt text](/client/src/images/dashboard.png?raw=true)




